/**
 * @file comu.h
 * @author LCY
 * @date 2020.03.09
 * @brief 通訊相關函式定義。
 */

#ifndef COMU_H
#define COMU_H

#include <stdint.h>

#include "Descriptors.h"


/**
 * @brief uart初始化函式
 *
 */
void uart_init(void);

/**
 * @brief uart反初始化函式
 *
 */
void uart_deinit(void);

/**
 * @brief uart送出資料函式
 *
 * @param c 欲傳出資料
 */
void uart_put(uint8_t c);

#endif /* COMU_H */