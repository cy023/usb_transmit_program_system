/**
 * @file iostate.c
 * @author LCY
 * @author LiYu87@mvmc-lab
 * @date 2020.03.09
 * @brief atmega16U4相關設定以及狀態偵測實作。
 */

#include "iostate.h"

#include <avr/io.h>
#include <stddef.h>

void gpio_init(void) {
    /**
     * PORTF
     *   - PF7 (I): ASAID 旋鈕 bit 3
     *   - PF6 (I): ASAID 旋鈕 bit 2
     *   - PF5 (I): ASAID 旋鈕 bit 1
     *   - PF4 (I): ASAID 旋鈕 bit 0
     *   - PF3 (X)
     *   - PF2 (X)
     *   - PF1 (I): detect Master/Slave mode.
     *   - PF0 (I): detect     Prog/Run mode.
     */
    DDRF = 0b00000000;

    /**
     * PORTD
     *   - PD7 (O): Prog/Run mode output for U1(m3/m128).
     *   - PD6 (O): Master/Slave mode output for multiplexers.
     *   - PD5 (O): Use in Slave mode, CS output for U1(m3/m128).
     *   - PD4 (X):
     *   - PD3 (O): TX, controll by uart1.
     *   - PD2 (I): RX, controll by uart1.
     *   - PD1 (I): (EXTINT) Use in Slave mode, detect CS from ASABUS.
     *   - PD0 (I/O):
     *       - I: (EXTINT) Detect switch reset signal.
     *       - O: reset u1. normal high.
     */
    DDRD = 0b11101000;

    /**
     * PORTC
     *   - PC7 (O): TX LED.
     *   - PC6 (O): RX LED.
     *   - PC5~0 (X)
     */
    DDRC = 0b11000000;

    /**
     * PORTB
     *   - PB7 (X)
     *   - PB6 (I): detect ASABUS ID bit 2
     *   - PB5 (I): detect ASABUS ID bit 1
     *   - PB4 (I): detect ASABUS ID bit 0
     *   - PB3~0 (X)
     */
    DDRB = 0b00000000;
}

inline uint8_t asa_id_bus(void) {
    return ((PINB & 0b01110000) >> 4);
}

inline uint8_t asa_id_sw(void) {
    return ((PINF & 0b11110000) >> 4);
}

inline uint8_t is_asa_id_matched(void) {
    return asa_id_bus() == asa_id_sw();
}

uint8_t m_s_detect(void) {
    if (PINF & (1 << PF1)) {
        return MS_MODE_MASTER;
    }
    else {
        return MS_MODE_SLAVE;
    }
}

void m_s_update(uint8_t mode) {
    switch (mode) {
        case MS_MODE_SLAVE: {
            // PIN: M/S = Low
            PORTD &= ~(1 << PD6);

            if (is_asa_id_matched()) {
                PORTD &= ~(1 << PD5);
            }
            else {
                PORTD |=  (1 << PD5);
            }
            break;
        }
        case MS_MODE_MASTER: {
            // PIN: M/S = High
            PORTD |= (1 << PD6);
            break;
        }
    }
}

uint8_t p_r_detect(void) {
    if (PINF & (1 << PF0)) {
        return PR_MODE_PROG;
    }
    else {
        return PR_MODE_RUN;
    }
}

void p_r_update(uint8_t mode) {
    switch (mode) {
        case PR_MODE_PROG: {
            // Pro/Run_To_128 pull high
            PORTD |= (1 << PD7);
            break;
        }
        case PR_MODE_RUN: {
            // Pro/Run_To_128 pull low
            PORTD &= ~(1 << PD7);
            break;
        }
    }
}

void tx_led_on(void) {
    PORTC &= ~(1 << PC7);
}

void rx_led_on(void) {
    PORTC &= ~(1 << PC6);
}

void tx_led_off(void) {
    PORTC |= (1 << PC7);
}

void rx_led_off(void) {
    PORTC |= (1 << PC6);
}


/**
 * pull down time 
 *   = 1 / f_cpu  * N
 *   = 1 / 16 MHz * 15
 *   = 1e-6 s
 *   = 1 us
 */
void reset_u1(void) {
    // set PD0 as output, and pull down it
    DDRD |= (1 << PD0);
    PORTD &= ~(1 << PD0);

    // Consume 15 clock to pull down
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");
    __asm__("nop");

    // pull up PD0, and reset it to output
    PORTD |= (1 << PD0);
    DDRD &= ~(1 << PD0);
}

void to_prog_u1(void) {
    // set PD7 as output
    DDRD |= (1 << PD7);
    // /Run_To_128 pull high
    PORTD &= ~(1 << PD7);
}

void to_run_u1(void) {
    // set PD7 as output
    DDRD |= (1 << PD7);
    // Pro/Run_To_128 pull low
    PORTD |= (1 << PD7);
}