/**
 * @file packet_handler.h
 * @author LiYu87@mvmc-lab
 * @brief 處理封包解析的工作。
 * @date 2020.03.10
 */

#ifndef PAC_HANDLER_H
#define PAC_HANDLER_H

#include <inttypes.h>

#define P1_CMD_RESET        1
#define P1_CMD_TO_PROG      2
#define P1_CMD_TO_RUN       3
#define P1_CMD_TO_RUN_RESET 4

#define SOFT_ON    0
#define SOFT_OFF   1

/**
 * @brief 封包處理用容器
 *
 * 將一個封包中主要、必要的資訊包裝起來，如此一來，就只需要使用這個結構來管理封包，
 * 只需要管理命令、資料、以及資料長度即可，不需要再自行打包封包，降低後續應用撰寫的
 * 複雜度。
 *
 * @param cmd    為封包的命令。
 * @param data   為資料的 buffer，需要自行指向一個空間。
 * @param length 為資料長度，相關處理函式，會依照這個長度去讀取 data 指標。
 * 
 */
typedef struct __pac_handler_t {
    uint8_t type;
    uint8_t cmd;
    uint16_t length;
    uint8_t *data;
} PacContainer_t;

/**
 * @brief 解包器結構
 * 
 * @param state    狀態紀錄。
 * @param counter  計數器。
 * @param w_p      寫入資料用指標。
 * @param pac_p    封包資訊儲存。
 */
typedef struct __pac_decoder_t {
    uint8_t state;
    uint8_t tmp;
    uint16_t counter;
    uint8_t *w_p;
    PacContainer_t *pac_p;
} PacDecoder_t;

/**
 * @brief 執行一次解包器動作
 *
 * @param p 解包器。
 * @param ch 輸入字元。
 * @return uint8_t
 *    - 0 成功無誤。
 *    - 1 代表錯誤。
 */
uint8_t packet_decoder_step(PacDecoder_t *p, uint8_t ch);

/**
 * @brief 解包器是否解包完成
 * 
 * @param p 解包器結構
 * @return uint8_t 
 *    - 0 否
 *    - 1 是
 * 
 * 解包完成，解包器會被鎖住，此函式會回傳 1 。
 * 拿取完資料後，需要呼叫 packet_decoder_unlock 來解鎖解包器。
 */
uint8_t packet_decoder_is_done(PacDecoder_t *p);

/**
 * @brief 解包器解鎖
 * 
 * @param p 解包器結構
 * @return uint8_t 
 *    - 0 成功無誤，並更改解包狀態為 HEADER。
 *    - 1 代表錯誤，目前解包狀態不在 DONE。
 */
uint8_t packet_decoder_unlock(PacDecoder_t *p);

/**
 * @brief 解析在P1協定下接收到之命令
 * 
 * @return 命令代號
 */
uint8_t p1_cmd_decode(const PacContainer_t *p);

#endif  /* PAC_HANDLER_H */
