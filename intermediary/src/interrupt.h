/**
 * @file interrupt.h
 * @author LCY
 * @brief 中斷相關函式定義。
 * @version 0.1
 * @date 2020-03-24
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef INTERRUPT_H
#define INTERRUPT_H

#include <stdint.h>
#include "Descriptors.h"

/**
 * @brief UART中斷初始化
 * 
 */
void uart_int_init(void);

/**
 * @brief UART中斷反初始化
 *
 */
void uart_int_deinit(void);

/**
 * @brief 計時中斷初始化函式。
 *
 * 使用timer1：
 *   - mode: CTC
 *   - prescaler: 8
 *   - OCR  = 9999
 *   - F_tim = 16000000 / (2 * prescaler * (1 + OCR))
 *           = 100 hz
 *   - F_int = F_tim * 2
 *           = 200 hz
 */
void tim_init(void);

/**
 * @brief 外部中斷初始化函式。
 *
 * 初始化 PD0、PD1 的外部中斷功能。
 * PD0 (EXT0) 偵測reset訊號，使用上緣觸發。
 * PD1 (EXT1) 偵測ASABUS CS訊號，使用上下緣觸發。
 */
void ex_init(void);

#endif /* INTERRUPT_H */