/**
 * @file packet_handler.c
 * @author LiYu87
 * @brief 封包處理函式的實作
 * @date 2019.12.10
 */

#include "packet_handler.h"
#include <string.h>

// DECODER 解包器用 macro
#define STATE_HEADER    0
#define STATE_DONE      1
#define STATE_P1_ARGS   10
#define STATE_P2_CMD    20
#define STATE_P2_TOCKEN 21
#define STATE_P2_LENGTH 22
#define STATE_P2_DATA   23
#define STATE_P2_CHKSUM 24
#define STATE_P3        30
#define STATE_P3_TOCKEN 31
#define STATE_P3_LENGTH 32
#define STATE_P3_DATA   33
#define STATE_P3_CHKSUM 34

uint8_t packet_decoder_step(PacDecoder_t *p, uint8_t ch) {
    switch (p->state) {
        case STATE_HEADER: {
            if (ch == '!') {
                p->state = STATE_P1_ARGS;
                p->w_p = p->pac_p->data;
                p->counter = 0;
            }
            else if (ch == 0xAA) {
                p->state = STATE_P2_CMD;
                p->pac_p->type = 1;
                p->w_p = p->pac_p->data;
                p->counter = 0;
            }
            else if (ch == 0xFC) {
                p->counter++;
                if (p->counter == 3) {
                    p->state = STATE_P3;
                }
            }
            else {
                p->counter = 0;
            }
            break;
        }
        case STATE_P1_ARGS: {
            if (ch == ' ') {
                *(p->w_p) = '\0';
                p->counter++;
                p->w_p = p->pac_p->data + p->counter * 10;
            }
            else if (ch == '\n' || ch == '\r') {
                *(p->w_p) = '\0';
                p->state = STATE_DONE;
                p->pac_p->length = p->counter + 1;
                
                p->counter = 0;
            }
            else {
                *(p->w_p) = ch;
                p->w_p ++;
            }
            break;
        }
        case STATE_P2_CMD: {
            p->pac_p->cmd = ch;
            p->state = STATE_P2_TOCKEN;
            break;
        }
        case STATE_P2_TOCKEN: {
            if (ch == 0x01) {
                p->state = STATE_P2_LENGTH;
            }
            else {
                p->state = STATE_HEADER;
            }
            break;
        }
        case STATE_P2_LENGTH: {
            if (p->counter == 0) {
                p->pac_p->length = ch << 8;
                p->counter++;
            }
            else {
                p->pac_p->length += ch;
                p->state = STATE_P2_DATA;
                p->counter = 0;
            }
            break;
        }
        case STATE_P2_DATA: {
            p->pac_p->data[p->counter] = ch;
            p->counter++;
            if (p->counter == p->pac_p->length) {
                p->state = STATE_P2_CHKSUM;
            }
            break;
        }
        case STATE_P2_CHKSUM: {
            if (p->tmp != ch) {
                p->state = STATE_HEADER;
            } else {
                p->state = STATE_DONE;
            }
            break;
        }
        case STATE_DONE: {
            // NOTE
            // lock decoder write access here
            // util call decoder_unlock()
            return 1;
        }
        default: {
            return 1;
        }
    }
    return 0;
}

uint8_t packet_decoder_is_done(PacDecoder_t *p) {
    if (p->state == STATE_DONE) {
        return 1;
    }
    else {
        return 0;
    }
}

uint8_t packet_decoder_unlock(PacDecoder_t *p) {
    if (p->state == STATE_DONE) {
        p->state = STATE_HEADER;
        return 0;
    }
    else {
        return 1;
    }
}


/**
 * P1 supprot command
 * 
 * 1. reset
 * 2. program+reset
 * 3. run
 * 4. run+reset
 */
uint8_t p1_cmd_decode(const PacContainer_t *p) {
    if (!strncmp((char *)p->data, "reset", 6)) {
        return P1_CMD_RESET;
    }
    if (!strncmp((char *)p->data, "prog", 6)) {
        return P1_CMD_TO_PROG;
    }
    if (!strncmp((char *)p->data, "run", 6)) {
        return P1_CMD_TO_RUN;
    }
    if(!strncmp((char *)p->data, "run+reset", 10)) {
        return P1_CMD_TO_RUN_RESET;
    }
    return 0;
}
