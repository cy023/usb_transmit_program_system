#include <stdio.h>
#include <string.h>

#include "packet_handler.h"

uint8_t buffer[300];

PacContainer_t pc = {
    .data = buffer
};

PacDecoder_t pd = {
    .pac_p = &pc
};

uint8_t test_1[] = "!cmd arg1 arg2 arg3\n";
uint8_t test_2[] = "!cmd2 arg4 arg5 arg6\n";

uint8_t test_3[7] = {
    0xAA, 0x01, 0x01, 0x00, 0x01, 0x10, 0x10
};

uint8_t test_4[9] = {
    0xAA, 0x05, 0x01, 0x00, 0x03, 0x04, 0x05, 0x06, 0x0C
};

int main(int argc, char const *argv[]) {
    printf("test packet handler\n");

    for (size_t i = 0; i < strlen(test_1); i++) {
        decoder_step(&pd, test_1[i]);
    }

    decoder_unlock(&pd);
    
    printf("pac_type is %d\n", pc.type);
    printf("%s\n", buffer);
    printf("%s\n", buffer + 10);
    printf("%s\n", buffer + 20);

    for (size_t i = 0; i < strlen(test_2); i++) {
        decoder_step(&pd, test_2[i]);
    }

    decoder_unlock(&pd);

    printf("pac_type is %d\n", pc.type);
    printf("%s\n", buffer);
    printf("%s\n", buffer + 10);
    printf("%s\n", buffer + 20);

    for (size_t i = 0; i < 7; i++) {
        decoder_step(&pd, test_3[i]);
        printf("ch is 0x%02X, pd state is %d\n", test_3[i], pd.state);
    }

    decoder_unlock(&pd);

    printf("pac_type is %d\n", pc.type);
    printf("pac command is %d\n", pc.cmd);
    
    for (size_t i = 0; i < pc.length; i++) {
        printf("0x%02X,", pc.data[i]);
    }

    for (size_t i = 0; i < 9; i++) {
        decoder_step(&pd, test_4[i]);
        printf("ch is 0x%02X, pd state is %d\n", test_4[i], pd.state);
    }

    decoder_unlock(&pd);

    printf("pac type is %d\n", pc.type);
    printf("pac command is %d\n", pc.cmd);
    
    for (size_t i = 0; i < pc.length; i++) {
        printf("0x%02X,", pc.data[i]);
    }

    return 0;
}

