DIR = intermediary

ifeq "$(wildcard intermediary/lib/lufa)" ""
all:
	cd $(DIR) && $(MAKE) get-lib
	cd $(DIR) && $(MAKE)
else

# Default target
all:
	cd $(DIR) && $(MAKE)

endif

clean:
	cd $(DIR) && $(MAKE) clean

fullclean:
	cd $(DIR) && $(MAKE) clean
	rm -r $(DIR)/lib || true